import { AddTaskComponent } from './add-task/add-task.component';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    AddTaskComponent
  ],
  imports: [
    CommonModule,
    IonicModule.forRoot()
  ]
})
export class ComponentsModule { }
