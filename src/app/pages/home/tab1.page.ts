import { TaskService } from '../../services/task.service';
import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/interfaces/interfaces';
import {
  AlertController,
  ModalController,
  ToastController,
} from '@ionic/angular';
import { AddTaskComponent } from 'src/app/components/add-task/add-task.component';
import { toastController } from '@ionic/core';
import * as moment from 'moment';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  tasks: Task[] = [];
  isLoading: boolean = true;
  taskForm: Task = {};

  constructor(
    private taskService: TaskService,
    private alertController: AlertController,
    private toatController: ToastController
  ) {}

  ngOnInit(): void {
    moment.locale('es');
    this.getTasks();
  }

  getTasks() {
    this.isLoading = true;
    this.taskService.getTasks().subscribe((res) => {
      res.forEach(item => {
        if (item.updated_at) {
          item.updated_dif = `Actualizado ${moment(item.updated_at).fromNow()}`;
        }
      })
      this.tasks = res;
      this.isLoading = false;
    });
  }

  taskChange(event: any, taskId: number) {
    this.isLoading = true;
    this.taskService.toggleStatus(taskId).subscribe(res => {
      this.getTasks();
    });
  }

  async showTask(task?: Task) {
    const alert = await this.alertController.create({
      header: task ? 'Actualizar tarea' : 'Nueva tarea',
      backdropDismiss: false,
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Nombre de la tarea',
          value: task ? task.name : null,
        },
        {
          name: 'description',
          type: 'textarea',
          placeholder: 'Descripción',
          value: task ? task.description : null,
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
        {
          text: task ? 'Actualizar' : 'Crear',
          handler: (value: { name: string; description: string }) => {
            if (value.name.trim() != '' && value.description.trim() != '') {
              if (task) {
                this.taskService
                  .updateTask({
                    ...task,
                    name: value.name,
                    description: value.description
                  })
                  .subscribe((res) => {
                    this.getTasks();
                  });
              } else {
                this.taskService
                  .createTask({
                    name: value.name,
                    description: value.description,
                    status: 0,
                  })
                  .subscribe((res) => {
                    this.getTasks();
                  });
              }
              toastController
                .create({
                  duration: 500,
                  message: `Tarea ${task ? 'actualizada' : 'creada'}`,
                })
                .then((toast) => {
                  toast.present();
                });
            } else {
              toastController
                .create({
                  duration: 500,
                  message: 'Ambos campos son obligatorios',
                })
                .then((toast) => {
                  toast.present();
                });
              return false;
            }
          },
        },
      ],
    });

    alert.present();
  }
}
