import { Task } from './../interfaces/interfaces';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const BASE_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})

export class TaskService {

  constructor(private http: HttpClient) { }


  getTasks()  {
    return this.http.get<Task[]>(`${BASE_URL}/tasks`);
  }

  createTask(task: Task) {
    return this.http.post<Task>(`${BASE_URL}/tasks`, task);
  }

  updateTask(task: Task) {
    return this.http.put<Task>(`${BASE_URL}/tasks/${task.id}`, task);
  }

  toggleStatus(taskId: number) {
    return this.http.delete(`${BASE_URL}/tasks/${taskId}`);
  }

}
