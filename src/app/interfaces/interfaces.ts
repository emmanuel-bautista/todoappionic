export interface Task {
  id?:          number;
  name?:        string;
  description?: string;
  status?:      number;
  created_at?:  string;
  updated_at?:  string;
  updated_dif?:  string;
}
